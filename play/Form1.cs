﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace play
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            File.Delete("array.txt");
            SoundPlayer utano = new SoundPlayer(Properties.Resources.grey);
            utano.PlayLooping();
        }
        int?[,] strategy1;
        int?[,] strategy2;
        int Points = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int player1strategy = Convert.ToInt32(strategyPlayer1.Value);
            if (player1strategy < 1)
                player1strategy = 1;
            int player2strategy = Convert.ToInt32(strategyPlayer2.Value);
            if (player2strategy < 1)
                player2strategy = 1;
            strategy1 = new int?[player1strategy, player2strategy];
           
            strategy2 = new int?[player1strategy, player2strategy];
            dataGridView1.ColumnCount = player1strategy;
            dataGridView1.RowCount = player2strategy;
            for (int i = 0; i < player1strategy; i++)
            {
                dataGridView1.Columns[i].HeaderCell.Value
                       = (i + 1).ToString();
                dataGridView1.Columns[i].Width = 60;
                for (int j = 0; j < player2strategy; j++)
                {
                    strategy1[i, j] = rand.Next(18) - 9;
                    strategy2[i, j] = rand.Next(18) - 9;
                    dataGridView1[i,j].Value = $"{strategy2[i, j]};{strategy1[i, j]}";
                    dataGridView1.Rows[j].HeaderCell.Value
                       = (-(j + 1)).ToString();
                }
            }
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            button1.Visible = false;
            buttonPlay.Visible = true;
            strategyPlayer2.Minimum = -100;
            strategyPlayer1.Minimum = -100;
            this.Text = $"Points: {Points}";
            label1.Text = "Колонка 1";
            label2.Text = "Колонка 2";
            writeInTxt();
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            int trueStrategy1 = Convert.ToInt32(strategyPlayer1.Value);
            int trueStrategy2 = Convert.ToInt32(strategyPlayer2.Value);
            if ((trueStrategy1 < 0 && trueStrategy2<0) || (trueStrategy2 > 0 && trueStrategy1 > 0))
            {
                compareStrategy(trueStrategy1, trueStrategy2);
                writeInTxt();
            }
        }
        bool compareStrategy(int i, int j)
        {
            if (i < 0 || j < 0)
            {
                for (int n = 0; n < strategy2.GetLength(1); n++)
                {
                    if (strategy2[n, Math.Abs(i + 1)] == null || strategy2[n, Math.Abs(j + 1)] == null)
                        continue;
                    if (strategy2[n, Math.Abs(i + 1)] < strategy2[n, Math.Abs(j + 1)])
                    {
                        Points -= 10;
                        this.Text = $"Points: {Points}";
                        return false;
                    }
                }
                for (int n = 0; n < strategy2.GetLength(0); n++)
                {
                    strategy1[Math.Abs(j + 1), n] = null;
                    strategy2[Math.Abs(j + 1), n] = null;
                }
                dataGridView1.Rows[Math.Abs(j + 1)].Visible = false;
                Points += 10;
                this.Text = $"Points: {Points}";
                return true;

            }
            else
            {
                for (int n = 0; n < strategy1.GetLength(0); n++)
                {
                    if (strategy1[i - 1, n] == null || strategy1[j - 1, n] == null)
                        continue;
                    if (strategy1[i - 1, n] < strategy1[j - 1, n])
                    {
                        Points -= 10;
                        this.Text = $"Points: {Points}";
                        return false;
                    }
                }
                dataGridView1.Columns[j - 1].Visible = false;
                Points += 10;
                for (int n = 0; n < strategy1.GetLength(0); n++)
                {
                    strategy2[j - 1, n] = null;
                    strategy1[j - 1, n] = null;
                }
                this.Text = $"Points: {Points}";
                return true;
            }

        }
        void writeInTxt()
        {
            
            using (var tw = new StreamWriter("array.txt", true))
            {
                string s = "\n";
                for (int i = 0; i < strategy1.GetLength(0); i++)
                {
                    for (int j = 0; j < strategy1.GetLength(1); j++)
                    {
                        if(strategy1[j, i] != null)
                        s += $"{strategy2[j, i]};{strategy1[j, i]} ";
                    }
                        
                    tw.WriteLine(s);
                    s = "";
                }
                tw.WriteLine("");
            }

        }
    }
}
